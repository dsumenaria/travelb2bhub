<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AdImages Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Ads
 *
 * @method \App\Model\Entity\AdImage get($primaryKey, $options = [])
 * @method \App\Model\Entity\AdImage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AdImage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AdImage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AdImage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AdImage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AdImage findOrCreate($search, callable $callback = null, $options = [])
 */
class AdImagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('ad_images');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->belongsTo('Ads', [
            'foreignKey' => 'ad_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->requirePresence('link', 'create')
            ->notEmpty('link');

        $validator
            ->requirePresence('image', 'create')
            ->notEmpty('image');

        

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['ad_id'], 'Ads'));

        return $rules;
    }
}
