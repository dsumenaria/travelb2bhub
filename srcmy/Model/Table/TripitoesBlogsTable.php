<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TripitoesBlogs Model
 *
 * @method \App\Model\Entity\TripitoesBlog get($primaryKey, $options = [])
 * @method \App\Model\Entity\TripitoesBlog newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TripitoesBlog[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesBlog|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TripitoesBlog patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesBlog[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesBlog findOrCreate($search, callable $callback = null, $options = [])
 */
class TripitoesBlogsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tripitoes_blogs');
        $this->displayField('title');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description');
            $validator
            ->requirePresence('short_description', 'create')
            ->notEmpty('short_description');
          
        $validator
            ->requirePresence('image_url', 'create')
            ->notEmpty('image_url');

        return $validator;
    }
}
