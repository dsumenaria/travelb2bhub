<style>
h1,h2,h3,h4,h5,h6{
	font-family: 'Raleway', sans-serif !important;
}
li { list-style: none;}
.review-block .block-text {
    background-color: #eee;
    padding: 10px 15px;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
}
.carousel-inner {
    position: relative;
    width: 100%;
    overflow: hidden;
}
.carousel {
    position: relative;
}
.review-block .block-text p {
    margin: 0;
    min-height: 120px; 
    height: 120px;
    z-index: 30;
}
hr { margin-top:0px!important;}
.bg-red{
	background-color:#db7474 !important
}
.bg-blue-2{
	background-color:#64bfe3 !important;
}
.bg-green{
	background-color:#688ea9 !important;
}
.bg-yellow{
	background-color:#f99696 !important;
}
.bg-white
{
	padding-top: 10px !important;
    background-color: #232323;
    padding-right: 10px !important;
    padding-left: 10px !important;
}
.arroysign
{
	margin: 17px;
	right: 23px !important;
    width: 3% !important;
    top: 40%;
    bottom: 52%;
}
.rating i {
	color:#1295A2 !important;
}

</style>
<?php 
$lastword=  substr($_SERVER['REQUEST_URI'], strrpos($_SERVER['REQUEST_URI'], '/') + 1);
	//if($lastword=="dashboard"  ) {
		?>
		<div class="portalmobile">
		<div class="portalmobile">
			<div class="row">
			<?php
			if($users['role_id'] == 1 || $users['role_id'] == 2)
			{
				?>
				<div class="col-md-6">
				<a href="<?php echo $this->Url->build(array('controller'=>'users','action'=>'sendrequest')) ?>">
					<li class="col-lg-12 col-xs-12 tile   tile-1 slideTextUp">
					  <!-- small box -->
					  <div class="small-box bg-red">
						<div class="inner" style="padding:0 !important;">
							<table style="margin: 28px 25px;">
								<tr>
									<td>
										<?php echo $this->Html->image('white-place-request-icon.png',array('style'=>'width:40px; height:30px;')); ?>
									</td>
								</tr>
								<tr>
									<td style="font-size: 18px;text-align: center;font-weight: 600;line-height: 14px;padding-top: 10px;padding-bottom: 10px;">
										<?php echo $PlaceReqCount; ?>
									</td>
								</tr>
								<tr>
									<td style="text-align: center;font-size: 10px;line-height: 12px;letter-spacing: 2px;text-transform: uppercase;">Place Request</td>
								</tr>
							</table>		
							 
						</div>
					  </div>
					  <div class="small-box bg-white">
						  <span>Click here to fill your Requirement for Travel Package, Hotel or Transportation. </span> 
 					  </div>
					</li>
				</a>
				</div>
				<!-- ./col -->
				<div class="col-md-6" >
				<a href="<?php echo $this->Url->build(array('controller'=>'users','action'=>'requestlist')) ?>">
					<li class="col-lg-12 col-xs-12 tile   tile-1 slideTextUp">
					  <!-- small box -->
					  <div class="small-box bg-yellow">
						<div class="inner" style="padding:0 !important;">
							<table style="margin: 20px 32px;">
								<tr>
									<td>
										<?php echo $this->Html->image('white-my-request-icon.png',array('style'=>'width: 36px;height: 40px;')); ?>
									</td>
								</tr>
								<tr>
									<td style="font-size: 18px;text-align: center;font-weight: 600;line-height: 14px;padding-top: 10px;padding-bottom: 10px;">
										<?php echo $myRequestCountNew; ?>
									</td>
								</tr>
								<tr>
									<td style="text-align: center;font-size: 10px;line-height: 12px;letter-spacing: 2px;text-transform: uppercase;">My Requests</td>
								</tr>
							</table>		
						</div>
					  </div>
					  <div class="small-box bg-white">
						<span> Click here to view the list of all currently Open requests placed by you. <span> 
 					  </div>
					</li>
				</a>
				</div>
			<?php 
			} 
			if($users['role_id'] == 1 || $users['role_id'] == 3) { 
			?>
				<!-- COls -->
				<div class="col-md-6">
				<a href="<?php echo $this->Url->build(array('controller'=>'users','action'=>'respondtorequest')) ?>">
					<li class="col-lg-12 col-xs-12 tile   tile-1 slideTextUp">
					  <!-- small box -->
					  <div class="small-box bg-green">
						<div class="inner" style="padding:0 !important;">
							<table style="margin: 20px 20px;">
								<tr>
									<td>
										<?php echo $this->Html->image('white-back-icon.png',array('style'=>'width: 36px;height: 40px;')); ?>
									</td>
								</tr>
								<tr>
									<td style="font-size: 18px;text-align: center;font-weight: 600;line-height: 14px;padding-top: 10px;padding-bottom: 10px;">
										<?php echo $respondToRequestCountNew; ?>
									</td>
								</tr>
								<tr>
									<td style="text-align: center;font-size: 10px;line-height: 12px;letter-spacing: 2px;text-transform: uppercase;">Respond to Requests</td>
								</tr>
							</table>		
							 
						</div>
					  </div>
					  <div class="small-box bg-white">
						<span> Click here to view, And Respond to Requirements placed by other users. <span> 
 					  </div>
					</li>
				</a>
				</div>
				<!---COls--->
				<div class="col-md-6" >
				<a href="<?php echo $this->Url->build(array('controller'=>'users','action'=>'myresponselist')) ?>">
					<li class="col-lg-12 col-xs-12 tile   tile-1 slideTextUp">
					  <!-- small box -->
					  <div class="small-box bg-blue-2">
						<div class="inner" style="padding:0 !important;">
							<table style="margin: 18px 28px;">
								<tr>
									<td>
										<?php echo $this->Html->image('white-my-resposes-head.png',array('style'=>'width:40px; height:40px;')); ?>
									</td>
								</tr>
								<tr>
									<td style="font-size: 18px;text-align: center;font-weight: 600;line-height: 14px;padding-top: 10px;padding-bottom: 10px;">
										<?php echo $myReponseCountNew; ?>
									</td>
								</tr>
								<tr>
									<td style="text-align: center;font-size: 10px;line-height: 12px;letter-spacing: 2px;text-transform: uppercase;">My Responses</td>
								</tr>
							</table>		
							 
						</div>
					  </div>
					  <div class="small-box bg-white">
						<span> Click here to view all currently open Requests, You have Respoded to. <span> 
 					  </div>
					</li>
				</a>
				</div>
		<?php }
		
		if($users['role_id'] == 2 ) { 
			?>
		
				<!-- COls -->
				<div class="col-md-6" >
				<a href="<?php echo $this->Url->build(array('controller'=>'users','action'=>'finalized-request-list')) ?>">
					<li class="col-lg-12 col-xs-12 tile   tile-1 slideTextUp">
					  <!-- small box -->
					  <div class="small-box bg-green">
						<div class="inner">
							<table style="margin: 15px 24px;">
								<tr>
									<td>
										<i style='font-size:46px' class="fa fa-check-square"></i>
									</td>
								</tr>
								<tr>
									<td style="font-size: 18px;text-align: center;font-weight: 600;line-height: 14px;padding-top: 10px;padding-bottom: 10px;">
										<?php echo $finalizeRequestNew; ?>
									</td>
								</tr>
								<tr>
									<td style="text-align: center;font-size: 10px;line-height: 12px;letter-spacing: 2px;text-transform: uppercase;">Finalized Requests</td>
								</tr>
							</table>		
							 
						</div>
					  </div>
					  <div class="small-box bg-white">
						<span> Click here to view, And Respond to Requirements placed by other users. <span> 
 					  </div>
					</li>
				</a>
				</div>
				<!---COls--->
				<div class="col-md-6" >
				<a href="<?php echo $this->Url->build(array('controller'=>'users','action'=>'removed-request-list')) ?>">
					<li class="col-lg-12 col-xs-12 tile   tile-1 slideTextUp">
					  <!-- small box -->
					  <div class="small-box bg-blue">
						<div class="inner">
							<table style="margin: 15px 24px;">
								<tr>
									<td>
										<i style='font-size:46px' class="fa fa-trash"></i>
									</td>
								</tr>
								<tr>
									<td style="font-size: 18px;text-align: center;font-weight: 600;line-height: 14px;padding-top: 10px;padding-bottom: 10px;">
										<?php echo $RemovedReqestNew; ?>
									</td>
								</tr>
								<tr>
									<td style="text-align: center;font-size: 10px;line-height: 12px;letter-spacing: 2px;text-transform: uppercase;">Removed Requests</td>
								</tr>
							</table>		
							 
						</div>
					  </div>
					  <div class="small-box bg-white">
						<span> Click here to view all currently open Requests, You have Respoded to. <span> 
 					  </div>
					</li>
				</a>
				</div>
		<?php }
			if($users['role_id'] == 3)
			{
				?>
				<div class="col-md-6" >
				<a href="<?php echo $this->Url->build(array('controller'=>'users','action'=>'my-final-responses')) ?>">
					<li class="col-lg-12 col-xs-12 tile   tile-1 slideTextUp">
					  <!-- small box -->
					  <div class="small-box bg-red">
						<div class="inner">
							<table style=" margin: 14px;">
								<tr>
									<td>
										<i style='font-size:46px' class="fa fa-check-square"></i>
									</td>
								</tr>
								<tr>
									<td style="font-size: 18px;text-align: center;font-weight: 600;line-height: 14px;padding-top: 10px;padding-bottom: 10px;">
										<?php echo $FInalResponseCountNew; ?>
									</td>
								</tr>
								<tr>
									<td style="text-align: center;font-size: 10px;line-height: 12px;letter-spacing: 2px;text-transform: uppercase;">Finalized Responses</td>
								</tr>
							</table>		
							 
						</div>
					  </div>
					  <div class="small-box bg-white">
						  <span>Click here to fill your Requirement for Travel Package, Hotel or Transportation. </span> 
 					  </div>
					</li>
				</a>
				</div>
				<!-- ./col -->
				<div class="col-md-6" >
				<a href="<?php echo $this->Url->build(array('controller'=>'users','action'=>'blocked-user-list')) ?>">
					<li class="col-lg-12 col-xs-12 tile   tile-1 slideTextUp">
					  <!-- small box -->
					  <div class="small-box bg-yellow">
						<div class="inner">
							<table style="margin: 0px 24px;">
								<tr>
									<td>&nbsp;	<br> 
										<?php echo $this->Html->image('blockuser.png',array('style'=>'width:40px; height:40px;')); ?>
									</td>
								</tr>
								<tr>
									<td style="font-size: 18px;text-align: center;font-weight: 600;line-height: 14px;padding-top: 10px;padding-bottom: 10px;">
										<?php echo $blockedUserscountnew; ?>
									</td>
								</tr>
								<tr>
									<td style="text-align: center;font-size: 10px;line-height: 12px;letter-spacing: 2px;text-transform: uppercase;">Blocked Users</td>
								</tr>
							</table>		
						</div>
					  </div>
					  <div class="small-box bg-white">
						<span> Click here to view the list of all currently Open requests placed by you. <span> 
 					  </div>
					</li>
				</a>
				</div>
			<?php 
			}
		?>
	  </div>
	</div> 	 
</div>	 