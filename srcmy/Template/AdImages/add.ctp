<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Ad Images'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="adImages form large-9 medium-8 columns content">
    <?= $this->Form->create($adImage) ?>
    <fieldset>
        <legend><?= __('Add Ad Image') ?></legend>
        <?php
            echo $this->Form->input('ad_id');
            echo $this->Form->input('title');
            echo $this->Form->input('link');
            echo $this->Form->input('image');
            echo $this->Form->input('created_on');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
