<section class="content">
<div class="row">
	<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<i class="fa fa-list"></i> <b>Tripitoes Users Report</b>
				</div> 
				<div class="box-body"> 
					<table class="table table-bordered" cellpadding="0" cellspacing="0" id="main_tble">
						<thead>
							<tr>
								<th scope="col"><?= $this->Paginator->sort('S.No') ?></th>
								<th scope="col"><?= h('provider') ?></th>
								<th scope="col"><?= h('Name') ?></th>
								<th scope="col"><?= h('Gender') ?></th>
								<th scope="col"><?= h('Email') ?></th>
								<th scope="col"><?= h('Phone') ?></th>
								<th scope="col"><?= h('Created on') ?></th>
								<th scope="col"><?= h('City') ?></th> 

								<th scope="col"><?= h('Image') ?></th>   
							</tr>
						</thead>
						<tbody>
							<?php  $x=0; foreach ($TripitoesUsers as $membership): ?>
							<tr>
								<td><?= h(++$x) ?></td>
								<td><?= h($membership->provider) ?></td>
								<td><?= h($membership->display_name) ?></td>
								<td><?= h($membership->gender) ?></td>
								<td><?= h($membership->email_verified) ?></td>
								<td><?= h($membership->phone) ?></td>
								<td><?= h(date('d-m-Y h:i:s',strtotime($membership->created))) ?></td>
								<td><?= h($membership->city) ?></td>

								<td><?= $this->Html->image($membership->photo_url,['style'=>'width:50px;height:49px;']) ?></td>  
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
					<div class="paginator">
						<ul class="pagination">
							<?= $this->Paginator->prev('< ' . __('previous')) ?>
							<?= $this->Paginator->numbers() ?>
							<?= $this->Paginator->next(__('next') . ' >') ?>
						</ul>
						<p><?= $this->Paginator->counter() ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
