<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $tripitoesBlog->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesBlog->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Blogs'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="tripitoesBlogs form large-9 medium-8 columns content">
    <?= $this->Form->create($tripitoesBlog) ?>
    <fieldset>
        <legend><?= __('Edit Tripitoes Blog') ?></legend>
        <?php
            echo $this->Form->input('title');
            echo $this->Form->input('description');
            echo $this->Form->input('created_on');
            echo $this->Form->input('image_url');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
