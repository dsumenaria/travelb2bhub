<style>
.select2-container--default .select2-results__option[aria-disabled=true] {
    display: none;
}

.hr{
	margin-top:25px !important;
}
	
a:hover,a:focus{
    outline: none !important;
    text-decoration: none !important;
}
.tab .nav-tabs{
    display: inline-block !important;
    background: #F0F0F0 !important;
    border-radius: 50px !important;
    border: none !important;
    padding: 1px !important;
}
.tab .nav-tabs li{
    float: none !important;
    display: inline-block !important;
    position: relative !important;
}
.tab .nav-tabs li a{
    font-size: 16px !important;
    font-weight: 700 !important;
    background: none !important;
    color: #999 !important;
    border: none !important;
    padding: 10px 15px !important;
    border-radius: 50px !important;
    transition: all 0.5s ease 0s !important;
}
.tab .nav-tabs li a:hover{
    background: #1295A2 !important;
    color: #fff !important;
    border: none !important;
}
.tab .nav-tabs li.active a,
.tab .nav-tabs li.active a:focus,
.tab .nav-tabs li.active a:hover{
    border: none !important;
    background: #1295A2 !important;
    color: #fff !important;
}
.tab .tab-content{
    font-size: 14px !important;
    color: #686868 !important;
    line-height: 25px !important;
    text-align: left !important;
    padding: 5px 20px !important;
}
.tab .tab-content h3{
    font-size: 22px !important;
    color: #5b5a5a !important;
} 
fieldset{
	margin:10px !important;
	border-radius: 6px;
} 
</style>
<div class="container-fluid">

<div class="box box-primary">
	<div class="box-body">
		<?= $this->Form->create($postTravlePackage,['type'=>'file','id'=>'TaxtEDIT']);?>
			<div class="row"> 
				<div class="col-md-12"> 
					<div class="form-box">
						<fieldset>
							<legend style="color:#369FA1;"><b><?= __('Load Package Promotion') ?></b></legend>
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-4 form-group">
											<p for="from">
												Enter Package Title
												<span class="required">*</span>
											</p>
											<div class="input-field">
												 <?php echo $this->Form->input('title',['class'=>'form-control requiredfield','label'=>false,'placeholder'=>"Enter Package Title",'required']);?>
												 <label style="display:none" class="helpblock error" > This field is required.</label>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
<div class="loader-wrapper" style="width: 100%;height: 100%;  display: none;  position: fixed; top: 0px; left: 0px;    background: rgba(0,0,0,0.25); display: none; z-index: 1000;" id="loader-1">
	<div id="loader"></div>
</div>
<div class="tripshow" style="display:none">
	<div class="col-md-12">
		<div class="col-md-4 form-group">
			<p for="from">
				Select Hotel Class   <span class="required">*</span>
			</p>
			<div class="input-field">
			 <select name="hotel_class" class="form-control requiredfield" placeholder="Select...">
				<option value="">Select...</option>
				<option <?php if($postTravlePackage->hotel_class == 1 ){ echo 'selected="selected"' ;} ?> value="1">&#9733;</option>
				<option <?php if($postTravlePackage->hotel_class == 2 ){ echo 'selected="selected"'; } ?>value="2">&#9733;&#9733;</option>
				<option <?php if($postTravlePackage->hotel_class == 3 ){ echo 'selected="selected"'; } ?>value="3">&#9733;&#9733;&#9733;</option>
				<option <?php if($postTravlePackage->hotel_class == 4 ){ echo 'selected="selected"'; } ?>value="4">&#9733;&#9733;&#9733;&#9733;</option>
				<option <?php if($postTravlePackage->hotel_class == 5 ){ echo 'selected="selected"'; } ?>value="5">&#9733;&#9733;&#9733;&#9733;&#9733;</option>
			</select>
			<label style="display:none" class="helpblock error" > This field is required.</label>
			</div>
		</div>
		<div class="col-md-4 form-group">
			<p for="from">
				B2C Package Price (per pax)
				<span class="required">*</span>
			</p>
			<div class="input-field">
				 <?php echo $this->Form->input('trip_price',['class'=>'form-control requiredfield','label'=>false,'placeholder'=>"Enter B2C Package Price",'oninput'=>"this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');",'type'=>'text']);?>
				 <label style="display:none" class="helpblock error" > This field is required.</label>
			</div>
		</div>
		
		<div class="col-md-2 form-group pull-right">
		<?php 
			if(!empty($postTravlePackage->trip_image))
			{
				echo $this->Html->image($cdn_path.$postTravlePackage->trip_image, ["class"=>"img-responsive",'style'=>"width: 57px; height:53px"]);
			}
			else{
				echo $this->Html->image('user_docs/noimage.png', ["class"=>"img-responsive",'style'=>"width: 57px; height:53px"]);
			}	 
		?>  
		</div>
	</div>
</div>

<?php echo $this->Html->script('/assets/plugins/jquery/jquery-2.2.3.min.js'); ?>
<script>
 $(document).ready(function () {
 
<?php if($countryLists[0]!=101){?> 
	$(".replacedata").html('<?php $options=array();
	foreach($countries as $country)
	{
		if($country->id==101){
			continue;
		}
		if(in_array($country->id, $countryLists)){
			$options[] = ['value'=>$country->id,'text'=>$country->country_name,'selected'=>'selected'];
		}
		else
		{
			$options[] = ['value'=>$country->id,'text'=>$country->country_name];
		}
	};
	echo $this->Form->input('country_id',["class"=>"form-control select2 requiredfield cntry",'required', "multiple"=>true ,'options' => $options,'label'=>false,"data-placeholder"=>"Select Countries "]);?> <label id="country-id-error" for="country-id" style="display:none" class="helpblock error" > This field is required.</label>');

	<?php } else { ?>
	 
		$(".replacedata").html('<?php $options=array();
		$options[] = ['value'=>'101','text'=>'India','selected'];
		echo $this->Form->input('country_id',["class"=>"form-control select2 requiredfield cntry",'required',"multiple"=>true ,'options' => $options,'label'=>false]);
	?> <label style="display:none" for="country-id" id="country-id-error" class="helpblock error" > This field is required.</label>');
		 

	<?php } ?>
	$('.select2').select2();
	//------ COUNTERY
  	var countries = '<?php echo $countryListsARYY;?>';
  	var Cty_id = '<?php echo $cityListsAry;?>';
 
	var m_data = new FormData();
	m_data.append('country_id',countries);			
	m_data.append('Cty_id',Cty_id);			
	$.ajax({
		url: "<?php echo $this->Url->build(["controller" => "PostTravlePackages", "action" => "ajaxCityEdit"]); ?>",
		data: m_data,
		processData: false,
		contentType: false,
		type: 'POST',
		dataType:'text',
		success: function(data)
		{
			$('#mcity').html('<select name="city_id[]" multiple="multiple" class="form-control select2 requiredfield max_limit" required data-placeholder="Select City" id="multi_city" style="height:125px;">'+data+'</select><label id="multi_city-error" style="display:none" class="error" for="multi_city">This field is required.</label>');
			$("#multi_city").select2();
		}
	});
});
</script>
<script>
 	  
    $(document).ready(function () {
			/* var pack_type=$('#pack_type').val();
			//alert(pack_type);
			if(pack_type==1){
				$(".replacedata").html('<?php $options=array();
				foreach($countries as $country)
				{
					if($country->id==101){
						continue;
					}
					$options[] = ['value'=>$country->id,'text'=>$country->country_name];
				};
				echo $this->Form->input('country_id',["class"=>"form-control select2 requiredfield cntry", "multiple"=>true ,'options' => $options,'label'=>false,"data-placeholder"=>"Select Countries "]);?> <label style="display:none" class="helpblock error" > This field is required.</label>');
			}
			else{
				$(".replacedata").html('<?php $options=array();
				$options[] = ['value'=>'101','text'=>'India','selected'];
				echo $this->Form->input('country_id',["class"=>"form-control select2 requiredfield cntry","multiple"=>true ,'options' => $options,'label'=>false]);
				?> <label style="display:none" class="helpblock error" > This field is required.</label>');
			}
			$('.select2').select2();
			 */
		<?php if($postTravlePackage->share==1){ ?>
			$('.tripdata').html($('.tripshow').html());
		<?php } ?>
		
		$(document).on('click',".share",function(){ 
			 
			var radioValue = $("input[name='share']:checked").val();

			if(radioValue==1){ 
				$('.tripdata').html($('.tripshow').html());
			}
			else
			{
				$('.tripdata').html('');
			}
		});	

		
		$(document).on('change','.cntry',function()
		{
			var country_id=$('option:selected', this).val();
			var countries = [];
			$.each($(".cntry option:selected"), function(){
				 countries.push($(this).val());
			});
         		 
		    var m_data = new FormData();
			m_data.append('country_id',countries);			
			$.ajax({
				url: "<?php echo $this->Url->build(["controller" => "PostTravlePackages", "action" => "ajax_city"]); ?>",
				data: m_data,
				processData: false,
				contentType: false,
				type: 'POST',
				dataType:'text',
				success: function(data)
				{
					$('#mcity').html('<select name="city_id[]" multiple="multiple" class="form-control select2 requiredfield max_limit" data-placeholder="Select City" id="multi_city" required style="height:125px;">'+data+'</select> <label id="multi_city-error" style="display:none" class="error" for="multi_city">This field is required.</label>');
					$("#multi_city").select2();
				}
			});
		});	
		
		$(document).on('change','.priceMasters',function()
		{
			var blank=$(this).val();
			var priceVal=$('.priceMasters option:selected').attr('priceVal');
			var price=$('.priceMasters option:selected').attr('price');
			if(blank!=''){
			var Result = priceVal.split(" ");
			var Result1 = price.split(" ");
			var weeks=Result[0];
			var price=Result1[0];
			
			var todaydate = new Date(); // Parse date
			for(var x=0; x < weeks; x++){
				todaydate.setDate(todaydate.getDate() + 7); // Add 7 days
			}
			var dd = todaydate .getDate();
			var mm = todaydate .getMonth()+1; //January is 0!
			var yyyy = todaydate .getFullYear();
			if(dd<10){  dd='0'+dd } 
			if(mm<10){  mm='0'+mm } 
			var date = dd+'-'+mm+'-'+yyyy;	
			$('.visible_date').val(date);
			$('.payment_amount').val(price);
			}
			else{
				//$('.visible_date').val('dd-mm-yyyy');
				$('.payment_amount').val(0);
			}
		});
		$(document).on('change','#pack_type',function()
		{
			var pack_type=$(this).val();
			//alert(pack_type);
			 
			if(pack_type==1){
				$('#mcity').html('<select name="city_id[]" multiple="multiple" class="form-control select2 requiredfield max_limit" data-placeholder="Select City" id="multi_city" required style="height:125px;"><option value="">Select Please </option></select><label id="multi_city-error" style="display:none" class="error" for="multi_city">This field is required.</label>');
				$(".replacedata").html('<?php $options=array();
				foreach($countries as $country)
				{
					if($country->id==101){
						continue;
					}
					$options[] = ['value'=>$country->id,'text'=>$country->country_name];
				};
				echo $this->Form->input('country_id',["class"=>"form-control select2 requiredfield cntry", "required", "multiple"=>true ,'options' => $options,'label'=>false,"data-placeholder"=>"Select Countries "]);?><label style="display:none" class="helpblock error" for="country-id" > This field is required.</label>');
			}
			else{
				$('#mcity').html('<select name="city_id[]" multiple="multiple" class="form-control select2 requiredfield max_limit" data-placeholder="Select City" required id="multi_city" style="height:125px;"><option value="">Select Please </option></select><label id="multi_city-error" style="display:none" class="error" for="multi_city">This field is required.</label>');
					$("#multi_city").select2();
				$(".replacedata").html('<?php $options=array();
				$options[] = ['value'=>'101','text'=>'India'];
				echo $this->Form->input('country_id',["class"=>"form-control select2 requiredfield cntry",'required',"multiple"=>true ,'options' => $options,'label'=>false]);
				?> <label style="display:none" for="country-id" class="helpblock error" > This field is required.</label>');
			}
			$('.select2').select2();
		});
$("#multi_city").multiselect();
$("#multi_states").multiselect();
$("#multi_category").multiselect();
});
</script>
<script type="text/javascript">
    function checkCertificate() {
        var fuData = document.getElementById('hotelImg');
        var FileUploadPath = fuData.value;
        if (FileUploadPath == '') {
            alert("Please upload an image");
        } else {
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
			if ( Extension == "png" ||  Extension == "jpeg" || Extension == "jpg") {

                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 
		else {
                alert("Photo only allows file types of PNG, JPG and JPEG.");
				$("#hotelImg").val('');
            }
        }
    }
</script>
<?php echo $this->Html->script(['jquery.validate']);?>
<script>
$('#TaxtEDIT').validate({
		rules: {
			"image" : {
				required : false,
			}
		}, 
		submitHandler: function (form) {
 			$("#loader-1").show();
			form[0].submit(); 
		}
	});
</script>