 
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                
                    <i class="fa fa-plus"></i> <b> Add Slider </b>
                 
            </div>
            <div class="box-body">
                <div class="form-group">    
                <?= $this->Form->create($tripitoesSlider,['id'=>'CityForm','type'=>'file']) ?>
                    <div class="row">
                        <div class="row col-md-6">
                            <div class="col-md-12">
                                <label class="control-label">Name  <span class="required" aria-required="true"> * </span></label>
                            </div>
                            <div class="col-md-12">
                                <?php echo $this->Form->input('name',[
                                'label' => false,'class'=>'form-control ','placeholder'=>'Enter name','style'=>'height:34px']);?>
                            </div>
                        </div>
                        <div class="row col-md-6">
                            <div class="col-md-12">
                                <label class="control-label">Location <span class="required" aria-required="true"> * </span></label>
                            </div>
                            <div class="col-md-12">
                                <?php echo $this->Form->input('location',[
                                'label' => false,'class'=>'form-control ','placeholder'=>'Enter Location','style'=>'height:34px']);?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">&nbsp;</div>
                    <div class="row">
                        <div class="row col-md-6">
                            <div class="col-md-12">
                                <label class="control-label">Link  <span class="required" aria-required="true"> * </span></label>
                            </div>
                            <div class="col-md-12">
                                <?php echo $this->Form->input('link',[
                                'label' => false,'class'=>'form-control ','placeholder'=>'Enter Link','style'=>'height:34px']);?>
                            </div>
                        </div>
                        <div class="row col-md-6">
                            <div class="col-md-12">
                                <label class="control-label">Image <span class="required" aria-required="true"> * </span></label>
                            </div>
                            <div class="col-md-12">
                                <?php echo $this->Form->input('image',[
                                'label' => false,'class'=>'form-control ','type'=>'file','id'=>'file']);?>
                                <span class="help-block">(1396 X 736) Image Resolution is valid
                                </span>
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-12">&nbsp;</div>
                     
                    <div class="box-footer">
                        <div class="row">
                            <center>
                            
                                <div class="col-md-12">
                                <hr></hr>
                                    <div class="col-md-offset-3 col-md-6">  
                                        <?php echo $this->Form->button('Submit',['class'=>'btn btn-primary','id'=>'submit_member']); ?>
                                    </div>
                                </div>
                            </center>       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>
</section>
<?php echo $this->Html->script('/assets/plugins/jquery/jquery-2.2.3.min.js'); ?>
<script>
$(document).ready(function() {
     
    // validate signup form on keyup and submit
     $("#CityForm").validate({ 
        rules: {
            name: {
                required: true
            },
            location: {
                required: true
            },
            image: {
                required: true
            },
            description: {
                required: true
            },
            link: {
                required: true
            }
        },
        submitHandler: function () {
            $("#submit_member").attr('disabled','disabled');
            form.submit();
        }
    }); 

});
</script>