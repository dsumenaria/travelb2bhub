<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ads Controller
 *
 * @property \App\Model\Table\AdsTable $Ads
 */
class AdsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['AdImages']
        ];
        $ads = $this->paginate($this->Ads);
        
        $this->set(compact('ads'));
        $this->set('_serialize', ['ads']);
    }

    /**
     * View method
     *
     * @param string|null $id Ad id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->layout('admin_layout');
        $this->paginate = [
            'contain' => ['AdImages']
        ];
        $ads = $this->paginate($this->Ads);
        $this->set(compact('ads'));
        $this->set('_serialize', ['ads']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->viewBuilder()->layout('admin_layout');
        $ad = $this->Ads->newEntity();
        $AdImages = $this->Ads->AdImages->newEntity();
        if ($this->request->is('post')) {

            $ad = $this->Ads->patchEntity($ad, $this->request->data);
            $ad->created_by=$this->Auth->User('id');
            $ad->is_active=1;
            $foreachdata=$this->request->data['ad_images'];
            
            if ($data=$this->Ads->save($ad)) {
                $insert_id=$data->id;
                $x=0;
                $this->Ads->AdImages->deleteAll(["ad_id"=>$insert_id]);
                foreach ($foreachdata as $value) {
                    $image=$value['image'];
                    if(!empty($image))
                    {   
                        $ext=explode('/',$image['type']);
                        $keyname = 'images/ads/'.time().rand().'.'.$ext[1];
                        $this->AwsFile->putObjectFile($keyname,$image['tmp_name'],$image['type']); 
                        $this->request->data['AdImages'][$x]['ad_id']=$insert_id;
                        $this->request->data['AdImages'][$x]['title']=$value['title'];
                        $this->request->data['AdImages'][$x]['link']=$value['link'];
                        $this->request->data['AdImages'][$x]['image']=$keyname;
                    }
                    $x++;
                }
                $SecondTable=$this->request->data['AdImages'];
                $adImages = $this->Ads->AdImages->newEntities($SecondTable);
                $this->Ads->AdImages->saveMany($adImages);
                $this->Flash->success(__('The ad has been saved.'));
                return $this->redirect(['action' => 'view']);
            }
            $this->Flash->error(__('The ad could not be saved. Please, try again.'));
        }
        $this->set(compact('ad'));
        $this->set('_serialize', ['ad']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ad id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    
    public function edit($id = null)
        {
            $ad = $this->Ads->get($id, [
                'contain' => []
            ]);
            if ($this->request->is(['patch', 'post', 'put'])) {
                $ad = $this->Ads->patchEntity($ad, $this->request->data);
                if ($this->Ads->save($ad)) {
                    $this->Flash->success(__('The ad has been saved.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('The ad could not be saved. Please, try again.'));
            }
            $this->set(compact('ad'));
            $this->set('_serialize', ['ad']);
        }
    public function active($id = null)
    {
        $ad = $this->Ads->get($id, [
            'contain' => []
        ]);
        $this->Ads->updateAll(['is_active' => 1], ['is_active' => 0]);
        $ad = $this->Ads->patchEntity($ad, $this->request->data);
        $ad->is_active=0;
        if ($this->Ads->save($ad)) {
            $this->Flash->success(__('Activeted successfully'));
            return $this->redirect(['action' => 'view']);
        }
        return $this->Flash->error(__('Something went wrong. Please, try again.'));         
    }
    /**
     * Delete method
     *
     * @param string|null $id Ad id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ad = $this->Ads->get($id);
        if ($this->Ads->delete($ad)) {
            $this->Ads->AdImages->deleteAll(["ad_id"=>$id]);
            $this->Flash->success(__('successfully Deleted.'));
        } else {
            $this->Flash->error(__('Something went wrong. Please, try again.'));
        }

        return $this->redirect(['action' => 'view']);
    }
}
