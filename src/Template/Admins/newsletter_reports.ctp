<section class="content">
<div class="row">
	<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<i class="fa fa-list"></i> <b>Newsletter Subscribe List</b>
				</div> 
				<div class="box-body"> 
					<table class="table table-bordered" cellpadding="0" cellspacing="0" id="main_tble">
						<thead>
							<tr>
								<th scope="col"><?= $this->Paginator->sort('S.No') ?></th>
								<th scope="col"><?= h('Email') ?></th>
								<th scope="col"><?= $this->Paginator->sort('created_on') ?></th>  
							</tr>
						</thead>
						<tbody>
							<?php $x=0; foreach ($tripitoesNewsletter as $membership): ?>
							<tr>
								<td><?= h(++$x) ?></td>
								<td><?= h($membership->email) ?></td>
								<td><?= h(date('d-m-Y h:i:s',strtotime($membership->created_on))) ?></td>  
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
					<div class="paginator">
						<ul class="pagination">
							<?= $this->Paginator->prev('< ' . __('previous')) ?>
							<?= $this->Paginator->numbers() ?>
							<?= $this->Paginator->next(__('next') . ' >') ?>
						</ul>
						<p><?= $this->Paginator->counter() ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
