<?php $cdn_path = $awsFileLoad->cdnPath(); ?>
<style>
hr { margin-top:0px!important;}
.price {
	height: 40px;
    background-color: #000000d9;
    color: #FFF;
    text-align: center;
    font-size: 18px;
    padding-top: 7px;
}
.priceing
{
    padding: 5px;
    margin-bottom: 20px;  
    border: 1px solid #ddd;
    border-radius: 5px;	
}
.img {
    position: relative;
    text-align: center;
    color: white;
}
.overlap {
    position: absolute;
    bottom: 0px;
    right: 0px;
	width:100%;
	opacity: .7;
}
.nm { 
	font-size: 19px;
    color: #373435;
    font-weight: 900;
}
.other { 
	font-size: 17px;
    color: #727376; 
}
.arroysign
{
	margin: 17px;
	right: 23px !important;
    width: 3% !important;
    top: 40%;
    bottom: 52%;
}
li > p{
		color:#96989A !important;
		margin: 0 0 4px !important;
	}	
 .button {
    width: 70%;
    font-size: 12px;
    max-width: 214px;
    text-align: center;
    color: rgb(255, 255, 255);
    background: none;
    border-width: 1px;
    border-style: solid;
    border-color: rgb(255, 255, 255);
    border-image: initial; 
    border-radius: 50px;
}
.button:hover{
	background-color:#FB6542;
} 

.btn-new{
	background-color: #252525 !important;
    color: #fff !important;
    border-color: #252525 !important;
    border-radius: 3px;
    letter-spacing: 1px;
    text-transform: uppercase;
    width: 70%;
    max-width: 116px
	}

.inner{
	color:#FFF !important;		
}
.textpormoition {
	font-size: 12px!important;
    letter-spacing: 1px;
} 
@media all and (max-width: 520px) {
	.content-header {
		padding: 0px 0px 0 0px !important;
	}
	.content{
	padding: 0px !important;
	}
	.tets{
		margin-top: 25px;
		margin-bottom: 52px;
	}
	.tets h4{
		padding-left: 10px;
	}
	    
}
@media all and (min-width: 520px) {
	.tets{
		margin-top: 25px;
		margin-bottom: 52px;
	}
}
.tbhight{height:140px;}
@media all and (min-width: 770px) {
	.small-box {margin-bottom: 0px !important;}
	.Dashbord{ margin-bottom:15px;}
	.tbhight{height:110px !important;}
}

@media all and (max-width: 767px){
	.margin_bottom{float: left;
    width: 100%;
    margin-bottom: 20px;}
}

.content{padding-top: 0px !important; margin-top: -7px !important;}
</style>

<section class="">
<div class="container-fluid">  
<?php 
	$role_id=$users->role_id ;
	//pr($users); exit;
 if($ShowPopup==1){ ?>
 	<div id="popup" class="modal fade in" role="dialog" style="display: block; padding-right: 20px;">
		<div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h5><font color="black"><b>Your profile is less than 70% complete.</b><br/><br/>
					 We request you to keep your information updated so other users can verify your authenticity. Click here to update your profile.<br/> <br/> 
					<b>Benefits for having an updated profile:</b><br/><br/>
					• Posts made by users with profiles more than 70% complete will show up at the top of the list of posts.<br/>
					• Users can only receive a Verified User tag on their posts once their profile is complete.<br/>
					• Only Verified Users will be permitted to connect and do business with international agents.<br/>
					</font></h5>
					
				</div>
				<div class="modal-footer">
					<?php $hrefurl =  $this->Url->build(array('controller'=>'users','action'=>'viewprofile',$users->id),1);?>
					<a href="<?php echo $hrefurl;?>" class="btn btn-danger">My Profile</a>
					<button type="button" class="btn btn-warning closeoclick" data-dismiss="modal">Skip</button>
				</div>
			</div>
		</div>
	</div>
	
<?php } ?>




 <div class="row equal_column tets" >
 <div class="col-md-8" style="padding:0 0;">
	 <div class="col-md-6">
		<?php 
		if($role_id==1) { ?>
			<h4 style="font-weight: 700; letter-spacing: 1px;">List or View Tourism Services</h4>
		<?php  } 
		if($role_id==3) { ?>
			<h4 style="font-weight: 700; letter-spacing: 1px;">List your Hotel or View Services</h4>
		<?php } 
		if($role_id==2) { ?>
			<h4 style="font-weight: 700; letter-spacing: 1px;">Promote your Company or View Services</h4>
		<?php } ?>

		<?php if($role_id==1) { ?>
		<div class="margin_bottom" style=""> 
		<li class="col-lg-12 col-xs-12 text-center Dashbord" style="/*background-image:url(../images/Travel.jpg);*/ background: #4bb7e1; height:33%;width:100%;background-repeat:round;">
			  <!-- small box -->
			  <div class="" >
				<div class="inner" align="center" style="padding: 20px 0 !important;">
					<table border="0" class="tbhight" style="text-align: center;">
						<tr>
							<td colspan="2" style="font-size:17px; letter-spacing: 2px; padding-bottom: 10px;"><img src="../images/package.png" style="width:50px" /></td>	
						</tr>
						<tr>
							<td colspan="2" style="font-size:17px; letter-spacing: 2px;"><b>Listed Packages</b></td>	
						</tr>
						<tr>
							<td colspan="2" class="textpormoition" style="padding-bottom:10px;">Click on the buttons below to View or List Travel Packages </td>	
						</tr>
						 
						<tr>
							<td><a href="<?php echo $this->Url->build(array('controller'=>'PostTravlePackages','action'=>'report')) ?>">
							<button class="button btn-sm btn-new" >View </button></a></td>
							
							<td><a href="<?php echo $this->Url->build(array('controller'=>'PostTravlePackages','action'=>'add')) ?>">
								<button class="button btn-sm btn-new"  >Load </button></a></td>
							
						</tr>											 
					</table>	
				</div>
			  </div>
			   
			</li>
		</div>
		<?php } ?>
		<div class="margin_bottom neww" > 
			<li class="col-lg-12 col-xs-12 text-center Dashbord" style="/*background-image:url(../images/Hotel.jpg);*/ background:#74c88e; height:33%;width:100%;background-repeat:round;">
			  <!-- small box -->
			  <div class="" >
				<div class="inner" align="center" style="padding: 20px 0 !important;">
					<table border="0" class="tbhight" style="text-align: center;">
						<tr>
							<td colspan="2" style="font-size:17px; letter-spacing: 2px; padding-bottom: 10px;"><img src="../images/hotel.png" style="width:50px" /></td>	
						</tr>
						<tr>
							<td colspan="2" style="font-size:17px; letter-spacing: 2px;"><b>Listed Hotels</b></td>	
						</tr>
						<tr>
							<td colspan="2" class="textpormoition" style="padding-bottom:10px;">Click on the <?php if($role_id==3){ ?>buttons<?php } else{echo"button";} ?> below to View <?php if($role_id==3){ ?>or Post<?php } ?> Hotels </td>	
						</tr>
						 
						<tr>
							<td><a href="<?php echo $this->Url->build(array('controller'=>'HotelPromotions','action'=>'report')) ?>">
							<button <?php if($role_id!=3){ ?> style="width:35% !important;"<?php }?> class="button btn-sm btn-new" >View </button></a></td>
							
							<td><?php if($role_id==3){ ?><a href="<?php echo $this->Url->build(array('controller'=>'HotelPromotions','action'=>'add')) ?>">
								<button class="button btn-sm btn-new"  >Load </button></a><?php }?></td>
							
						</tr>											 
					</table>	
				</div>
			  </div>
			   
			</li>
		</div>
		
		<div class="margin_bottom">
		<li class="col-lg-12 col-xs-12 text-center Dashbord" style="/*background-image:url(../images/Taxi.jpg);*/ background:#fdd027; height:33%;width:100%;background-repeat:round; ">
			  <!-- small box -->
			  <div class="" >
				<div class="inner" align="center" style="padding: 20px 0 !important;">
					<table border="0" class="tbhight" style="text-align: center;">
						<tr>
							<td colspan="2" style="font-size:17px; letter-spacing: 2px; padding-bottom: 10px;"><img src="../images/transport.png" style="width:60px" /></td>	
						</tr>
						<tr>
							<td colspan="2" style="font-size:17px; letter-spacing: 2px;"><b>Listed Taxi/Fleet Services</b></td>	
						</tr>
						<tr>
							<td colspan="2" class="textpormoition" style="padding-bottom:10px;">Click on the <?php if($role_id==1){ ?>buttons<?php } else{echo"button";} ?> below to View <?php if($role_id==1){ ?> or List <?php } ?> Taxi/Fleet Services </td>	
						</tr>
						<tr>
							<td><a href="<?php echo $this->Url->build(array('controller'=>'TaxiFleetPromotions','action'=>'report')) ?>">
							<button <?php if($role_id!=1){ ?> style="width:50% !important;"<?php } ?> class="button btn-sm btn-new" >View </button></a></td>
							
							<td><?php if($role_id==1){ ?><a href="<?php echo $this->Url->build(array('controller'=>'TaxiFleetPromotions','action'=>'add')) ?>">
								<button class="button btn-sm btn-new"  >Load </button></a> <?php } ?></td>
						</tr>
					</table>	
				</div>
			  </div>
			</li>
		</div>
		<?php if($role_id==2 || $role_id==3) { ?>
		<div class="margin_bottom"> 
			<li class="col-lg-12 col-xs-12 text-center Dashbord" style="/*background-image:url(../images/Event.jpg);*/ background: #4bb7e1; height:33%;width:100%;background-repeat:round; ">
			  <!-- small box -->
			  <div class="small-box" >
				<div class="inner" align="center" style="padding: 20px 0 !important;">
					<table border="0" class="tbhight" style="text-align: center;">
						<tr>
							<td colspan="2" style="font-size:17px; letter-spacing: 2px; padding-bottom: 10px;"><img src="../images/event.png" style="width:50px" /></td>	
						</tr>
						<tr>
							<td colspan="2" style="font-size:17px; letter-spacing: 2px;"><b>Listed Event Planners</b></td>	
						</tr>
						<tr>
							<td colspan="2" class="textpormoition" style="padding-bottom:10px;">Click on the <?php if($role_id==2){ ?>buttons<?php } else{echo"button";} ?> below to View <?php if($role_id==2){ ?> or List <?php }?> Event Planning Services </td>	
						</tr>
						 
						<tr>
							<td><a href="<?php echo $this->Url->build(array('controller'=>'EventPlannerPromotions','action'=>'report')) ?>">
							<button <?php if($role_id!=2){ ?> style="width:35% !important;"<?php }?> class="button btn-sm btn-new" >View </button></a></td>
							
							<td><?php if($role_id==2){ ?><a href="<?php echo $this->Url->build(array('controller'=>'EventPlannerPromotions','action'=>'add')) ?>">
								<button class="button btn-sm btn-new"  >Load </button></a><?php }?></td>
							
						</tr>											 
					</table>	
				</div>
			  </div>
			   
			</li> 
		</div>
	<?php } ?> 
	
	</div>
		
	<div class='col-md-6 hideinphone' align="left">
		<div style="top: 28px;">
			<?php if($role_id ==1){ ?>
				<h4 style="font-weight: 700; letter-spacing: 1px;">Submit or Respond to Leads</h4>
				<p style="text-align:justify;margin-bottom: 22px !important;font-size: 12px;letter-spacing: 1px;">
				You can submit your client's customized requirements for Travel Packages, Hotel Rooms, or Transportation in the section below, and then get responses from interested Sellers. Similarly, you can also respond to queries by other buyers.
				</p>
			<?php }
			if($role_id ==3){ ?>
				<h4 style="font-weight: 700; letter-spacing: 1px;">Respond to Requirements </h4>
				<p style="text-align:justify;margin-bottom: 22px !important;font-size: 12px;letter-spacing: 1px;">
				Travel Companies will submit their client's requirements for Hotel Rooms, and you can respond to those queries if you are interested.
				</p>
			<?php }
			if($role_id ==2){ ?>
				<h4 style="font-weight: 700; letter-spacing: 1px;">Submit your Requirements</h4>
				<p style="text-align:justify;margin-bottom: 22px !important;font-size: 12px;letter-spacing: 1px;">
				You can submit your client's customized requirements for Travel Packages, Hotel Rooms, or Transportation in the section below, and then get responses from interested Sellers. 
				</p>
			<?php } ?>


			<?php echo $this->element('subheader');?> 
		</div>
		
		<div class="" style="background-color:#FFF;padding:20px;margin-bottom: 18px;">
			<h4 align="left" style="margin:0px"><b>Download the app</b> for a GREAT <b>user experience</b></h4><br>
			<p style="color:#5c5a5a;font-size:14px;">1) Receive instantaneous business notifications</p> 
			<p style="color:#5c5a5a;font-size:14px;">2) Real-time communication</p> 
			<p style="color:#5c5a5a;font-size:14px;">3) Mobile specific User Interface / User Experience</p> 
			<div align="left" style="margin-top:11px">
				<a target="_blank" href="https://play.google.com/store/apps/details?id=com.app.travel.TravelB2B"><?php echo  $this->Html->image('/images/google_play.png', ['style'=>'width:46%;']) ?></a>
			</div>
		</div>
	</div>
	 
<?php if($role_id!=2){ ?>
	<div class='col-md-12 hideinphone'>
		<div class="" style="background-color:#FFF; padding:2px 20px;">
		<?php if($role_id==1){ ?>
			<h4 align="left" style="padding-top:10px"><b>Now sell travel services to end customers (B2C) via </b>Tripitoes.com, <b>without leaving the B2B platform! </b></h4>
			<p>The packages, hotels, or taxi services you list at TravelB2BHub.com for B2B selling will also be made available on www.Tripitoes.com at your chosen B2C prices. The benefits are great!</p><br>
			<p style="color:#5c5a5a;font-size:14px;">1)	Unlimited access to B2C customers around India</p>
			<p style="color:#5c5a5a;font-size:14px;">2)	Dedicated B2C selling website </p>
			<p style="color:#5c5a5a;font-size:14px;">3)	Tripitoes is powered by TravelB2BHub, but keeps B2B and B2C customers separate to avoid pricing issues
			<p style="color:#5c5a5a;font-size:14px;">4)	No additional cost or commission!</p> 
		<?php } ?>
		<?php if($role_id==3){ ?>
 
			<h4 align="left" style="padding-top:10px"><b>Now sell Hotel rooms to end customers (B2C) via </b>Tripitoes.com, <b>without leaving the B2B platform! </b></h4><br>

			<p>The packages, hotels, or taxi services you list at TravelB2BHub.com for B2B selling will also be made available on www.Tripitoes.com at your chosen B2C prices. The benefits are great!</p><br>
			<p style="color:#5c5a5a;font-size:14px;">1)	Unlimited access to B2C customers around India</p>
			<p style="color:#5c5a5a;font-size:14px;">2)	List your direct reservations link for B2C users to make direct room bookings </p>
			<p style="color:#5c5a5a;font-size:14px;">3)	No additional cost or commission!</p>
			<p style="color:#5c5a5a;font-size:14px;">4)	Tripitoes is powered by TravelB2BHub, but keeps B2B and B2C customers separate to avoid pricing issues</p>  
		<?php } ?>
			<br>
		</div>
	</div>
	<?php } ?> 
</div>
	
	<div class="col-md-4">
		<h4 style="font-weight: 700; letter-spacing: 1px;float: left;"><?=$ads['sction_title']?></h4>
		<?php
		foreach ($ads['ad_images'] as $key => $value) {
		 
		?>
		<div style="background-image:url(<?= $cdn_path.$value['image']?>); margin-bottom: 16px; float:left; width: 100%;background-repeat: round;">
			<div style="float:left; width: 100%">
				<a href="http://<?=$value['link']?>" target="_blank">
					<div style="width: 100%;padding-top: 55px;top: 100px;color: #fff;text-align:left;">
						<h5 style="font-weight: 700;letter-spacing: 1px;margin-bottom: 0;margin-top: 70px;padding: 12px;background: #000000b0;"><?=$value['title']?></h5>
					</div>
				</a>
				
				<div id="imagemodal<?=$value['id']?>" class="modal fade" role="dialog">
					<div class="modal-dialog modal-md">
						<div class="modal-content">
							<div class="modal-body">
							<button type="button" class="close" data-dismiss="modal" style="padding-right:8px !important;">&times;</button>
							<img src="<?= $cdn_path.$value['image']?>" 
							style="width:100%;padding:20px;padding-top:0px!important;" alt=""/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>		
		
	</div>
</div>
	
	</div>
		
	</div>
	
</div>
				
</section>

	<!-- hide section for hotelier--->


<?php
	if($users['role_id'] != 3 && count($advertisement1) > 0){
	?>
		<div class="col-md-12" style="background-color:#FFF; margin-top:10px; display:none">
			<div style="padding:10px">
				<p style="font-size:20px;padding-top:10px;color:#4B4B4D">Hoteliers Interested in You</p>
			</div>
			<hr></hr>
				  
						<div id="myCarousel" class="carousel slide">
							<div class="carousel-inner">
							<?php
							$ad = $advertisement1;
							$i=1;
							foreach(array_chunk($advertisement1->toArray() ,3) as $advert){
								?>
								<div class="item <?php if($i==1){ echo 'active'; } ?>" >
									<div class="row-fluid">
									<?php
									$k =1;
									foreach($advert as $advert2){
										 
									if (!preg_match("~^(?:f|ht)tps?://~i",  $advert2['website'])) {
										$advert2['website'] = "http://" .  $advert2['website'];
									}
												 
									?>
										<div class=" col-md-4">
										<a onclick="countfunc('<?php echo $advert2['website']; ?>','<?php echo $advert2['id']; ?>')" href="<?php echo $advert2['website']; ?>" target="_blank">
											<div class="priceing">
												<div class="img" style="height:230px">
													<img 
													<?php if(($advert2['hotel_pic']=="") || (!file_exists('../img/hotels/'.$advert2['hotel_pic']))) {?> src="../img/blankhotel.PNG"<?php } else 
													{ ?>
													src="../img/hotels/<?php echo $advert2['hotel_pic']; } ?>" alt="Image" height="230px" width="100%" />
													<div class="price overlap" >Rs. <span>
														<?php echo $advert2['cheap_tariff']; ?></span>-<span><?php echo $advert2['expensive_tariff']; ?></span>
													</div>
												</div>
												<div class="" align="center">
												<table width="90%" border="0">
													<tr>
														<td height="60px">
															<span class="nm"><?php $nm= strtolower($advert2['hotel_name']); echo ucwords($nm); ?></span>
														</td>
													<tr>
													<tr>
														<td height="40px">
															 <span class="other">
															 <?php echo ucwords(strtolower(mb_strimwidth($advert2['hotel_location'], 0, 32, "..."))); ?>
														</span>
														</td>
													<tr>
													<tr>
														<td  height="40px">
															   
															<span class="other"><?php foreach($hotelCategories as $key=> $hotel){
																if($key==$advert2['hotel_type']){ echo ucwords(strtolower($hotel));}
															}
																?>
															</span>
														</td>
													<tr>
													</table> 
												</div>
											</div>
											</a>
										</div>
									<?php $k++;
									} ?>
									</div><!--/row-fluid-->
								</div><!--/item-->

							<?php $i++;
							} ?>
							</div><!--/carousel-inner-->

							<a class="left carousel-control arroysign" href="#myCarousel" data-slide="prev"><i class="fa fa-arrow-circle-left"></i></a>
							<a class="right carousel-control arroysign" href="#myCarousel" data-slide="next"><i class="fa fa-arrow-circle-right"></i></a>
						</div><!--/myCarousel-->

					</div><!--/well-->
				</div>
				</div>
              <?php } ?>
              
             
          </div>
    </div>
</div>
<?php echo $this->Html->script('/assets/plugins/jquery/jquery-2.2.3.min.js'); ?>
<script>
$(document).ready(function(){
    $(".closeoclick").on("click",function(){
		$('#popup').hide();
	});
});
function countfunc(web,id){
  $.ajax({
	url: '/users/promotioncounts/' + id,
	cache: false,
	type: 'GET',
	dataType: 'HTML',
	success: function () {
	  //location.href=web;
	  // window.open(web, '_blank');
	}
});
}
$(function() {
    $( "#element", this ).click(function( event ) {
        if( $(this).val().length >= 4 ) {
            $.ajax({
                url: '/clients/index/' + escape( $(this).val() ),
                cache: false,
                type: 'GET',
                dataType: 'HTML',
                success: function (clients) {
                    $('#clients').html(clients);
                }
            });
        }
    });
});
$('#carousel-reviews .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  
});
$('.carousel').carousel({
  interval:50000000000000000,
  ride:false
});

</script>