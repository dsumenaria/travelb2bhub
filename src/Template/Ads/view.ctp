<?php $cdn_path = $awsFileLoad->cdnPath(); ?>
<section class="content">
<div class="row">
    <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list"></i> <b>Advertisements List</b>
                </div> 
                <div class="box-body"> 
                    <table class="table table-bordered" cellpadding="0" cellspacing="0" id="main_tble">
                        <thead>
                            <tr>
                                <th scope="col"><?= $this->Paginator->sort('S.No') ?></th>
                                <th scope="col"><?= h('Section Title') ?></th>
                                <th scope="col"><?= h('Images') ?></th>
                                <th scope="col" class="actions"><?= __('Status') ?></th>
                                <th scope="col" class="actions"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $x=0; foreach ($ads as $membership): 
                            $rowspan=sizeof($membership->ad_images);
                            $active=$membership->is_active;
                            ?>
                            <tr>
                                <td><?= ++$x; ?></td>
                                <td><?= h($membership->sction_title) ?></td>
                                <td>
                                    <table class="table table-bordered" cellpadding="0" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th scope="col"><?= h('Title') ?></th>
                                            <th scope="col"><?= h('Link') ?></th> 
                                            <th scope="col"><?= h('Image') ?></th>
                                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                                        </tr>
                                        <?php
                                         foreach ($membership->ad_images as $key => $value) {
                                         ?>

                                        <tr>
                                            <td><?= h($value->title) ?></td>
                                            <td><?= h($value->link) ?></td> 
                                            <td>
                                                <?php  
                                                echo $this->Html->image($cdn_path.$value->image, ["class"=>"img-responsive",'style'=>"width: 60px; height:60px"]);?>
                                            </td> 
                                            <td class="actions">
                                                <a class=" btn btn-danger btn-xs" data-target="#deletemodal<?php echo $value->id; ?>" data-toggle=modal><i class="fa fa-trash"></i></a>
                                                <div id="deletemodal<?php echo $value->id; ?>" class="modal fade" role="dialog">
                                                    <div class="modal-dialog modal-md" >
                                                        <form method="post" action="<?php echo $this->Url->build(array('controller'=>'AdImages','action'=>'delete',$value->id)) ?>">
                                                            <div class="modal-content">
                                                              <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h4 class="modal-title">
                                                                    Are you sure you want to remove?
                                                                    </h4>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit" class="btn  btn-sm btn-info">Yes</button>
                                                                    <button type="button" class="btn  btn-sm btn-danger" data-dismiss="modal">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php 
                                        }
                                        ?>
                                 </table>
                                </td>
                                <td><?php if($active==0){ echo "Active"; } else { echo "Deactive";}?></td>
                                <td class="actions">
                                 <?php
                                 if($active==1){ 
                                    echo $this->Html->link('<i class="fa fa-check"></i>','/Ads/active/'.$membership->id,array('escape'=>false,'class'=>'btn btn-info btn-xs'));
                                    }
                                 ?>

                                <a class=" btn btn-danger btn-xs" data-target="#deletdasdsaemodal<?php echo $membership->id; ?>" data-toggle=modal><i class="fa fa-trash"></i></a>

                                <div id="deletdasdsaemodal<?php echo $membership->id; ?>" class="modal fade" role="dialog">
                                    <div class="modal-dialog modal-md" >
                                        <form method="post" action="<?php echo $this->Url->build(array('controller'=>'Ads','action'=>'delete',$membership->id)) ?>">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">
                                                    Are you sure you want to remove?
                                                    </h4>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn  btn-sm btn-info">Yes</button>
                                                    <button type="button" class="btn  btn-sm btn-danger" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                        </ul>
                        <p><?= $this->Paginator->counter() ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
