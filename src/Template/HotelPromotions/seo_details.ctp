<style>
.select2-container--default .select2-results__option[aria-disabled=true] {
    display: none;
}

.hr{
	margin-top:25px !important;
}
	
a:hover,a:focus{
    outline: none !important;
    text-decoration: none !important;
}
.tab .nav-tabs{
    display: inline-block !important;
    background: #F0F0F0 !important;
    border-radius: 50px !important;
    border: none !important;
    padding: 1px !important;
}
.tab .nav-tabs li{
    float: none !important;
    display: inline-block !important;
    position: relative !important;
}
.tab .nav-tabs li a{
    font-size: 16px !important;
    font-weight: 700 !important;
    background: none !important;
    color: #999 !important;
    border: none !important;
    padding: 10px 15px !important;
    border-radius: 50px !important;
    transition: all 0.5s ease 0s !important;
}
.tab .nav-tabs li a:hover{
    background: #1295A2 !important;
    color: #fff !important;
    border: none !important;
}
.tab .nav-tabs li.active a,
.tab .nav-tabs li.active a:focus,
.tab .nav-tabs li.active a:hover{
    border: none !important;
    background: #1295A2 !important;
    color: #fff !important;
}
.tab .tab-content{
    font-size: 14px !important;
    color: #686868 !important;
    line-height: 25px !important;
    text-align: left !important;
    padding: 5px 20px !important;
}
.tab .tab-content h3{
    font-size: 22px !important;
    color: #5b5a5a !important;
} 
fieldset{
	margin:10px !important;
	border-radius: 6px;
} 
</style>
<div class="container-fluid">

<div class="box box-primary">
	<div class="box-body">
		<?= $this->Form->create($hotelPromotion,['type'=>'file','id'=>'TaxtEDIT']);?>
			<div class="row"> 
				<div class="col-md-12"> 
					<div class="form-box">
						<fieldset>
							<legend style="color:#369FA1;"><b><?= __('SEO Details of '.$hotelPromotion->hotel_name) ?></b></legend>
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-12 form-group">
											<p for="from">
												Meta Title
												<span class="required">*</span>
											</p>
											<div class="input-field">
												 <?php echo $this->Form->input('seo_title',['class'=>'form-control requiredfield','label'=>false,'placeholder'=>"Enter Meta Title",'required','rows'=>2]);?>
												 <label style="display:none" class="helpblock error" > This field is required.</label>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-12 form-group">
											<p for="from">
												Meta  Description
												<span class="required">*</span>
											</p>
											<div class="input-field">
												 <?php echo $this->Form->input('seo_description',['class'=>'form-control requiredfield','label'=>false,'placeholder'=>"Enter Meta  Description",'required','type'=>'textarea']);?>
												 <label style="display:none" class="helpblock error" > This field is required.</label>
											</div>
										</div>
									</div>
									<div class="row">
									<div class="col-md-12">
										<div class="input-field">
											<div class="margin text-center">
											<center>
											<?php echo $this->Form->button('Submit',['class'=>'btn btn-primary btn-submit','value'=>'submit','style'=>'background-color:#1295A2']); ?>
											</center>
											</div>
										</div>
									</div> 
								</div>
								</div>
							</fieldset>
						</div>
					</div>
				</form>
			</div>
		<?= $this->Form->end();?>
		</div>
	</div>
<div class="loader-wrapper" style="width: 100%;height: 100%;  display: none;  position: fixed; top: 0px; left: 0px;    background: rgba(0,0,0,0.25); display: none; z-index: 1000;" id="loader-1">
	<div id="loader"></div>
</div> 

<?php echo $this->Html->script('/assets/plugins/jquery/jquery-2.2.3.min.js'); ?>

<?php echo $this->Html->script(['jquery.validate']);?>
<script>
$('#TaxtEDIT').validate({
		rules: {
			"image" : {
				required : false,
			}
		}, 
		submitHandler: function (form) {
 			$("#loader-1").show();
			form[0].submit(); 
		}
	});
</script>