<?php $cdn_path = $awsFileLoad->cdnPath(); ?>
<section class="content">
<div class="row">
    <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list"></i> <b>Slider List</b>
                </div> 
                <div class="box-body"> 
                    <table class="table table-bordered" cellpadding="0" cellspacing="0" id="main_tble">
                        <thead>
                            <tr>
                                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('location') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('link') ?></th>
                                <th scope="col"><?= ('Image') ?></th> 
                                <th scope="col" class="actions"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $l=0; foreach ($tripitoesSliders as $tripitoesSlider): ?>
							 
							<?php 
							 
							?>
                            <tr>
                                <td><?= ++$l; ?></td>
                                <td><?= h($tripitoesSlider->name) ?></td>
                                <td><?= h($tripitoesSlider->location) ?></td>
                                <td><?= h($tripitoesSlider->link) ?></td>
                                <td><?php  
                                    echo $this->Html->image($cdn_path.$tripitoesSlider->image_url, ["class"=>"img-responsive",'style'=>"width: 57px; height:53px"]);?>
                                </td> 
                                <td class="actions">  
                                    <a class=" btn btn-danger btn-xs" data-target="#deletemodal<?php echo $tripitoesSlider->id; ?>" data-toggle=modal><i class="fa fa-trash"></i></a>
                                    <div id="deletemodal<?php echo $tripitoesSlider->id; ?>" class="modal fade" role="dialog">
                                        <div class="modal-dialog modal-md" >
                                            <form method="post" action="<?php echo $this->Url->build(array('controller'=>'TripitoesSliders','action'=>'delete',$tripitoesSlider->id)) ?>">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">
                                                        Are you sure you want to remove this Slider?
                                                        </h4>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn  btn-sm btn-info">Yes</button>
                                                        <button type="button" class="btn  btn-sm btn-danger" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->first('<< ' . __('first')) ?>
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                            <?= $this->Paginator->last(__('last') . ' >>') ?>
                        </ul>
                        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                    </div>
                     
                </div>
            </div>
        </div>
    </div>
</section>
 
