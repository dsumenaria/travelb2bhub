 <?php $cdn_path = $awsFileLoad->cdnPath(); ?>
<section class="content">
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<?php if(!empty($id)){ ?>
					<i class="fa fa-pencil-square-o"></i> <b> Edit Blog </b>
				<?php }else{ ?>
					<i class="fa fa-plus"></i> <b> Add Blog </b>
				<?php } ?>
			</div>
			<div class="box-body">
				<div class="form-group">	
				<?= $this->Form->create($tripitoesBlog,['id'=>'CityForm','type'=>'file']) ?>
				<div class="row col-md-12" style="margin-top:10px">
					<div class="row col-md-6">
						<div class="col-md-12">
							<label class="control-label">Title <span class="required" aria-required="true"> * </span></label>
						</div>
						<div class="col-md-12">
							<?php echo $this->Form->input('title',[
							'label' => false,'class'=>'form-control ','placeholder'=>'Enter blog title','style'=>'height:34px','type'=>'text']);?>
						</div>
					</div>
					<div class="row col-md-6">
						<?php if(!$id){ ?>
						<div class="col-md-12">
							<label class="control-label">Image <span class="required" aria-required="true"> * </span></label>
						</div>
						<div class="col-md-12">
							<?php echo $this->Form->input('image_url',[
							'label' => false,'type'=>'file','class'=>'form-control ']);?>
						</div>
						<?php }else{
						 echo "<div align='center'>". $this->Html->image($cdn_path.$tripitoesBlog->image_url, ["class"=>"img-responsive",'style'=>"width: 80px; height:80px"]);
						 	echo"</div>"; 
						  } ?>
					</div>
				</div>
				<div class="row col-md-12" style="margin-top:10px">
					<div class="row col-md-12">
						<div class="col-md-12">
							<label class="control-label">Short Description <span class="required" aria-required="true"> * </span></label>
						</div>
						
						<div class="col-md-12">
							<textarea id="" name="short_description" class="form-control"><?php echo $tripitoesBlog->short_description;?></textarea>
							 
						</div>
					</div>
				</div>
				<div class="row col-md-12" style="margin-top:10px">
					<textarea id="description" name="description" hidden=""><?php echo $tripitoesBlog->description;?></textarea>
					<div class="col-md-12">
						<label class="control-label">Description </label>
					</div>
					<div class="col-md-12">
						<textarea class="txtEditor"><?php echo $tripitoesBlog->description;?></textarea>
					</div>
				</div> 
 					
				<div class="box-footer">
					<div class="row">
						<center>
							<div class="col-md-12">
							<hr></hr>
								<div class="col-md-offset-3 col-md-6">	
									<?php echo $this->Form->button('Submit',['class'=>'btn btn-primary','id'=>'submit_member']); ?>
								</div>
							</div>
						</center>		
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<?= $this->Form->end() ?>
</div>
</section>
<!--<script src='//tinymce.cachefly.net/4.2/tinymce.min.js'></script>

<script type='text/javascript'>
    tinymce.init({selector:'#description'});
</script>-->

<?php echo $this->Html->script('/assets/plugins/jquery/jquery-2.2.3.min.js'); ?>
<script>
$(document).ready(function() {
	$('.Editor-editor').html($('#description').text());
	$("button:submit").click(function(){  
 		$('#description').text($('.txtEditor').Editor("getText"));
	});
	
	// validate signup form on keyup and submit
	 $("#CityForm").validate({ 
		rules: {
			title: {
				required: true
			},
			image_url: {
				required: true
			},
			duration: {
				required: true
			},
			description: {
				required: true
			},
			short_description: {
				required: true
			}
		},
		submitHandler: function () {
			$("#submit_member").attr('disabled','disabled');
			form.submit();
		}
	}); 

});
</script>