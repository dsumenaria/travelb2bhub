 <?php $cdn_path = $awsFileLoad->cdnPath(); ?>
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-plus"></i> <b> Add Advertisement </b>
            </div>
            <div class="box-body">
                <div class="form-group">    
                <?= $this->Form->create($ad,['id'=>'CityForm','type'=>'file']) ?>
                <div class="row col-md-8" style="margin-top:10px">
                    <div class="col-md-12">
                        <label class="control-label">Section Title <span class="required" aria-required="true"> * </span></label>
                    </div>
                    <div class="col-md-12">
                        <?php echo $this->Form->input('sction_title',[
                        'label' => false,'class'=>'form-control ','placeholder'=>'Enter section title','style'=>'height:34px','maxlength'=>'35']);?>
                    </div>
                </div>
                <div class="row col-md-12" style="margin-top:10px">
                    <div class="col-md-12">
                    <table class="table table-bordered" id="main_table">    
                        <thead class="bg_color">
                            <tr align="">
                                <th style="text-align:left;">Sr</th>
                                <th style="text-align:left;width:15%">Post Title</th>
                                <th style="text-align:left;">Link</th> 
                                <th style="text-align:left;">Image(600*370)</th>
                                <th style="text-align:left;">Action</th>
                            </tr> 
                            
                        </thead>
                        <tbody id="main_tbody">
                            <tr class="main_tr">
                                <td width ="5%" style="vertical-align: top !important;"></td>
                                <td width="25%" align="left">
                                     <?php echo $this->Form->control('title', ['label' => false,'placeholder'=>'Post Title','class'=>'form-control title','required'=>'required','maxlength'=>'37']); ?>
                                </td>
                                <td width="25%" align="">
                                    <?php echo $this->Form->control('link', ['label' => false,'placeholder'=>'Link to Website','class'=>'form-control link','required'=>'required']); ?>
                                </td>
                                <td width="25%" class="">
                                    <?php echo $this->Form->input('image',['label' => false,'type'=>'file','class'=>'form-control image','required'=>'required']);?> 
                                </td>
                                <td  width="10%">
                                    <?php echo $this->Form->button($this->Html->tag('i', '', ['class'=>'fa fa-times']),['class'=>'btn  btn-danger btn-xs remove_row','type'=>'button']); ?>
                                    <?php echo $this->Form->button($this->Html->tag('i', '', ['class'=>'fa fa-plus']),['class'=>'btn  btn-info btn-xs add_row','type'=>'button']); ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
                     
                <div class="box-footer">
                    <div class="row">
                        <center>
                        
                            <div class="col-md-12">
                            <hr></hr>
                                <div class="col-md-offset-3 col-md-6">  
                                    <?php echo $this->Form->button('Submit',['class'=>'btn btn-primary','id'=>'submit_member']); ?>
                                </div>
                            </div>
                        </center>       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <?= $this->Form->end() ?>
</div>
</section>
<?php echo $this->Html->script('/assets/plugins/jquery/jquery-2.2.3.min.js'); ?>
<script> 
rename_rows();
 
$(document).on("click", ".add_row", function(e)
{
   //var tr =  $(this).closest('tr.main_tr').clone();
   var tr =  $('#cloan tbody#main_cloan .main_tr').clone();
    $("#main_table tbody#main_tbody").append(tr);
     rename_rows();
});
$(document).on("click", ".remove_row", function(e)
{ 
    var rowCount = $("#main_table tbody#main_tbody tr.main_tr").length; 
    if(rowCount>1)
    {
        $(this).closest("tr").remove();
        rename_rows();
    }
});
function rename_rows(){
    var i=0;
    $("#main_table tbody#main_tbody tr.main_tr").each(function(){
        var row_no = $(this).attr("row_no"); 
        $(this).find("td:nth-child(1)").html(i+1);

        $(this).find("td:nth-child(2) .title").attr({name:"ad_images["+i+"][title]"});

        $(this).find("td:nth-child(3) .link").attr({name:"ad_images["+i+"][link]"});
        
        $(this).find("td:nth-child(4) .image").attr({name:"ad_images["+i+"][image]"});
        i++;
    });
}
</script>
<script>
$(document).ready(function() {
    $('.Editor-editor').html($('#description').text());
    $("button:submit").click(function(){  
        $('#description').text($('.txtEditor').Editor("getText"));
    });
    
    // validate signup form on keyup and submit
     $("#CityForm").validate({ 
        rules: {
            title: {
                required: true
            },
            sction_title: {
                required: true
            },
            duration: {
                required: true
            },
            descriptionss: {
                required: true
            }
        },
        submitHandler: function () {
            $("#submit_member").attr('disabled','disabled');
            form.submit();
        }
    }); 

});

</script>

<table style="display:none" id="cloan">    
<tbody id="main_cloan">
    <tr class="main_tr">
        <td width ="5%" style="vertical-align: top !important;"></td>
        <td width="25%" align="left">
             <?php echo $this->Form->control('title', ['label' => false,'placeholder'=>'Post Title','class'=>'form-control title','required'=>'required','maxlength'=>'37']); ?>
        </td>
        <td width="25%" align="">
            <?php echo $this->Form->control('link', ['label' => false,'placeholder'=>'Link to Website','class'=>'form-control link','required'=>'required']); ?>
        </td>
        <td width="25%" class="">
            <?php echo $this->Form->input('image',['label' => false,'type'=>'file','class'=>'form-control image','required'=>'required']);?> 
        </td>
        <td  width="10%">
            <?php echo $this->Form->button($this->Html->tag('i', '', ['class'=>'fa fa-times']),['class'=>'btn  btn-danger btn-xs remove_row','type'=>'button']); ?>
            <?php echo $this->Form->button($this->Html->tag('i', '', ['class'=>'fa fa-plus']),['class'=>'btn  btn-info btn-xs add_row','type'=>'button']); ?>
        </td>
    </tr>
</tbody>
</table>