<style>
.unfollow{
	width:70px;
}
</style>
<div class="container-fluid" id="business_buddies_list">
<div class="row equal_column" > 
    <div class="col-md-12" > 
		 
 		<?php echo  $this->Flash->render() ?>
	</div>
</div>
 <div class="box box-primary">
		<div class="box-header with-border"> 
		<h3 class="box-title" style="padding:5px;">Following</h3>
			<div class="box-tools pull-right">
				<a style="font-size:22px" class="btn btn-box-tool" data-target="#myModal122" data-toggle="collapse"> <i class="fa fa-filter"></i></a>
			</div>
		</div>
	<div class="box-body">
	<div class="collapse"  id="myModal122" aria-expanded="false"> 
			<form class="filter_box" style="padding-right: 15px;padding-left: 15px;" method="get">
				<fieldset><legend style="text-align:left !important;">Filter</legend>
					<div class="">  
						<div class=col-md-6>
							<div >
								<label class="col-form-label" for=example-text-input>City</label>
							</div> 
							<div class="">
								<select class="form-control select2" name="city_id[]"  multiple="multiple">
								   <option value="">Select</option>
								   <?php foreach($allCities as $city){?>
								   <option value="<?php echo $city['value'];?>"><?php echo $city['label'];?></option>
								   <?php }?>
								</select>
							</div>
						</div>
						<div class=col-md-6>
							<div >
								<label class="col-form-label" for=example-text-input>State</label>
							</div> 
							<div class="">
								<select class="form-control select2" name="state_id[]" multiple="multiple" >
								   <option value="">Select</option>
								   <?php foreach($allstates as $citys){?>
								   <option value="<?php echo $citys['value'];?>"><?php echo $citys['label'];?></option>
								   <?php }?>
								</select>
							</div>
						</div> 						
					</div>
					
					<div class="col-md-12 text-center">
						<hr></hr>
						<a class="btn btn-danger btn-sm" href="<?php echo $this->Url->build(array('controller'=>'Users','action'=>'business-buddies-list')) ?>">Reset</a>
						<button class="btn btn-info btn-sm" name="submit" value="Submit" type="submit">Apply</button> 
					</div>
				</fieldset>
			</form>
		</div>
		<form method="get">
			<div class="" style="margin-bottom:5px;">
				<div class="row">
					<div class="col-md-12">
						<div class="">
							<table width="100%"><tr><td width="80%"> 
								<input class="form-control" placeholder="Name and Company Name" name="search"/></td>
								<td width="8%" style="padding-left:5px;"><button style="width:100%" class="btn btn-info btn-sm" name="submit" value="Submit" type="submit">Search</button></td>
								<td width="8%" style="padding-left:5px;"><a href="<?php echo $this->Url->build(array('controller'=>'Users','action'=>'business-buddies-list')) ?>"class="btn btn-danger btn-sm" style="width:100%">Reset</a>
								</td></tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form>
		<hr></hr> 
					<?php
						use Cake\Datasource\ConnectionManager; 
						$conn = ConnectionManager::get('default');
						if(count($BusinessBuddies) >0) {
							foreach($BusinessBuddies as $row){
									$total_rating=0;
									$rate_count=0;
									$final_rating=0;
									$sql1="Select * from `testimonial` where `user_id`='".$row['user']['id']."' ";
									$stmt1 = $conn->execute($sql1);
									foreach($stmt1 as $bresul){
										$rate_count++;
										$rating=$bresul['rating'];
										$total_rating+=$rating;
									} 
									if($total_rating>0){
										@$final_rating=$total_rating/$rate_count;
									}
									 
									?>
								<?php 
									$hrefurl =  $this->Url->build(array('controller'=>'users','action'=>'viewprofile',$row['user']['id']));
								?>
							 
					<div id="cat" >
						<div class="row">
							<div class="col-md-12">
							
								 <div class="col-md-5" style="margin-top: 5px !important;">
									<b>Name: </b> <a href="<?php echo $hrefurl; ?>"> <?php echo $row['user']['first_name']; ?>&nbsp;<?php echo $row['user']['last_name']; ?></a>
									<?php if($final_rating>0){ ?>
										<font color="#1295AB">(<?php echo round($final_rating); ?> <i class="fa fa-star"></i>)</font> 
									<?php } ?>
									<?php if($row['user']['isVerified']==1){  
										echo'<span class="verifiedCss"> &#10004; Verified </span> ';
									} ?>								
									<br>									
								</div>
								<div class="col-md-5" style="margin-top: 5px !important;">
								<b>City: </b> <?php echo ($row['user']['city']['name'].' ('.$row['user']['state']['state_name'].')')?$row['user']['city']['name'].' ('.$row['user']['state']['state_name'].')':"-- --"; ?></div>
								
								<div class="col-md-5" style="margin-top: 5px !important;">
								<b>Company Name: </b> <?php echo ($row['user']['company_name'])?$row['user']['company_name']:"-- --"; ?>
								</div>
								<div class="col-md-5" style="margin-top: 5px !important;">
								<b>Website: </b> <?php echo ($row['user']['web_url'])?$row['user']['web_url']:"-- --"; ?> 
								</div>
								<div class="col-md-2" align="left" style="margin-top: 5px !important;">
										<a follow_id="<?php echo $row['id']; ?>" class=" btn btn-danger btn-xs"  data-target="#unfollow<?php echo $row['id']; ?>" data-toggle=modal>Unfollow</a> 
									<!-------Delete Modal Start--------->
												<div id="unfollow<?php echo $row['id']; ?>" class="modal fade" role="dialog">
													<div class="modal-dialog modal-md" >
													<form method="post">
														<!-- Modal content-->
															<div class="modal-content">
															  <div class="modal-header"  >
																	<button type="button" class="close" data-dismiss="modal">&times;</button>
																	<h4 class="modal-title"  align="left">
																	Are you sure you want to unfollow this user?
																	</h4>
																</div>
																<div class="modal-footer" >
																	<button type="button" follow_id="<?php echo $row['id']; ?>" class="unfollow btn btn-info input-md" value="yes" >Yes</button>
																	<button type="button" class="btn btn-danger input-md" data-dismiss="modal">Cancel</button>
																</div>
															</div>
														</form>
													</div>
												</div>
											<!-------Delete Modal End--------->
											
								</div>
							</div>
						</div><hr></hr>
						<?php } ?>
					</div>
				</div>
			</div>
			
      <div class="pages"></div>
		<?php } else {?>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
				<div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 box-event test-center" align="center" style="text-align:center">
				You are not following anyone.
				</div>
			</div>
		<?php } ?>
    <!--<div class="col-md-5 col-xs-offset-7 right">
	<ul class="pagination">
    <li><a href="#">&laquo;</a></li>
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li><a href="#">&raquo;</a></li>
	</ul>
	</div>-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $this->Html->script('/assets/plugins/jquery/jquery-2.2.3.min.js'); ?>
<script>
$(document).ready(function () {
	$(".unfollow").click(function (e) {
		e.preventDefault();
		var url = "<?php echo $this->Url->build(array('controller'=>'users','action'=>'remove-business-buddy')) ?>";
		var follow_id = $(this).attr("follow_id");
		$.ajax({
			url:url,
			type: 'POST',
			data: {follow_id:follow_id}
		}).done(function(result){
			if(result == 1) {
				$('.modal').toggle();
				location.reload();
			} else {
				alert("There is some problem, please try again.");
			}
		});
		 
	});
});
</script>