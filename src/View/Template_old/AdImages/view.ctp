<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ad Image'), ['action' => 'edit', $adImage->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ad Image'), ['action' => 'delete', $adImage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $adImage->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ad Images'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ad Image'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="adImages view large-9 medium-8 columns content">
    <h3><?= h($adImage->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($adImage->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ad Id') ?></th>
            <td><?= $this->Number->format($adImage->ad_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= $this->Number->format($adImage->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created On') ?></th>
            <td><?= h($adImage->created_on) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Link') ?></h4>
        <?= $this->Text->autoParagraph(h($adImage->link)); ?>
    </div>
    <div class="row">
        <h4><?= __('Image') ?></h4>
        <?= $this->Text->autoParagraph(h($adImage->image)); ?>
    </div>
</div>
