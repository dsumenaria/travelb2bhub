<?php
namespace App\Controller\Api;
use Cake\Controller\Controller;
use Cake\Event\Event;
set_time_limit(0);
ini_set('memory_limit','2048M');
//use Cake\I18n\FrozenDate;
//use Cake\I18n\FrozenTime;
class AppController extends Controller
{
  use \Crud\Controller\ControllerTrait;
  public $components = [
        'RequestHandler','Flash',
        'Crud.Crud' => [ 
            'actions' => [
                'Crud.Index',
                'Crud.View',
                'Crud.Add',
                'Crud.Edit',
                'Crud.Delete'
            ],
            'listeners' => [
                'Crud.Api',
                'Crud.ApiPagination',
                'Crud.ApiQueryLog'
            ]
        ]
    ];
	public function initialize()
    {
		$coreVariable = [
			'SiteUrl' => 'http://localhost/travelb2bhub/',
			//'SiteUrl' => 'https://travelb2bhub.com/app/', 
		];
        $awsFileLoad=$this->loadComponent('AwsFile');
		$this->coreVariable = $coreVariable;
		$this->set(compact('awsFileLoad','coreVariable'));
	}
	
}
 
?>