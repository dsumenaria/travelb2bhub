<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TripitoesBlogs Controller
 *
 * @property \App\Model\Table\TripitoesBlogsTable $TripitoesBlogs
 */
class TripitoesBlogsController extends AppController
{
 
 
    public function view($id = null)
    {
        $this->viewBuilder()->layout('admin_layout');
        $tripitoesBlogs = $this->paginate($this->TripitoesBlogs->find()->where(['is_deleted'=>0]));

        $this->set(compact('tripitoesBlogs'));
        $this->set('_serialize', ['tripitoesBlogs']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id=null)
    {
		$this->viewBuilder()->layout('admin_layout');
        if(!$id){
            $tripitoesBlog = $this->TripitoesBlogs->newEntity();
        }
        else{
            $tripitoesBlog = $this->TripitoesBlogs->get($id, [
                'contain' => []
            ]);
        }
       if ($this->request->is(['patch', 'post', 'put'])){
            $tripitoesBlog = $this->TripitoesBlogs->patchEntity($tripitoesBlog, $this->request->data);
			
            if(!$id){
                $image=$this->request->data['image_url'];
    			$ext=explode('/',$image['type']);
                $keyname = 'img/blog_image/'.time().''.rand().'.'.$ext[1];
                $tripitoesBlog->image_url=$keyname;
                $this->AwsFile->putObjectFile($keyname,$image['tmp_name'],$image['type']);
			} 
            if ($this->TripitoesBlogs->save($tripitoesBlog)) {
                $this->Flash->success(__('Successfully Submitted.'));
                return $this->redirect(['action' => 'view']);
            }
            $this->Flash->error(__('Something went wrong. Please, try again.'));
        }
		
        $this->set(compact('tripitoesBlog','id'));
        $this->set('_serialize', ['tripitoesBlog','id']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tripitoes Blog id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tripitoesBlog = $this->TripitoesBlogs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tripitoesBlog = $this->TripitoesBlogs->patchEntity($tripitoesBlog, $this->request->data);
            if ($this->TripitoesBlogs->save($tripitoesBlog)) {
                $this->Flash->success(__('The tripitoes blog has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tripitoes blog could not be saved. Please, try again.'));
        }
        $this->set(compact('tripitoesBlog'));
        $this->set('_serialize', ['tripitoesBlog']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tripitoes Blog id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tripitoesBlog = $this->TripitoesBlogs->get($id);
		$tripitoesBlog = $this->TripitoesBlogs->patchEntity($tripitoesBlog, $this->request->data);
		$tripitoesBlog->is_deleted=1;
		if ($this->TripitoesBlogs->save($tripitoesBlog)) {
            $this->Flash->success(__('The tripitoes blog has been deleted.'));
        } else {
            $this->Flash->error(__('The tripitoes blog could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'view']);
    }
}
