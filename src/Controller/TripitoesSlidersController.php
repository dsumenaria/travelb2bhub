<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TripitoesSliders Controller
 *
 * @property \App\Model\Table\TripitoesSlidersTable $TripitoesSliders
 */
class TripitoesSlidersController extends AppController
{
   
    public function view()
    {
        $this->viewBuilder()->layout('admin_layout');
        $tripitoesSliders = $this->paginate($this->TripitoesSliders->find()->where(['is_deleted'=>0]));
		
        $this->set(compact('tripitoesSliders'));
        $this->set('_serialize', ['tripitoesSliders']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->viewBuilder()->layout('admin_layout');
        $tripitoesSlider = $this->TripitoesSliders->newEntity();
        if ($this->request->is('post')) {
            $tripitoesSlider = $this->TripitoesSliders->patchEntity($tripitoesSlider, $this->request->data);
            $image = $this->request->data('image'); 
            $ext=explode('/',$image['type']);
            $keyname = 'img/slider_image/'.time().''.rand().'.'.$ext[1];
            $tripitoesSlider->image_url=$keyname;
            $this->AwsFile->putObjectFile($keyname,$image['tmp_name'],$image['type']);
            if ($this->TripitoesSliders->save($tripitoesSlider)) {  
                $this->Flash->success(__('The slider has been saved.'));
                return $this->redirect(['action' => 'view']);
            }
            $this->Flash->error(__('The slider could not be saved. Please, try again.'));
        }
        $this->set(compact('tripitoesSlider'));
        $this->set('_serialize', ['tripitoesSlider']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tripitoes Slider id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tripitoesSlider = $this->TripitoesSliders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tripitoesSlider = $this->TripitoesSliders->patchEntity($tripitoesSlider, $this->request->data);
            if ($this->TripitoesSliders->save($tripitoesSlider)) {
                $this->Flash->success(__('The tripitoes slider has been saved.'));

                return $this->redirect(['action' => 'view']);
            }
            $this->Flash->error(__('The tripitoes slider could not be saved. Please, try again.'));
        }
        $this->set(compact('tripitoesSlider'));
        $this->set('_serialize', ['tripitoesSlider']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tripitoes Slider id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $tripitoesSlider = $this->TripitoesSliders->get($id, [
            'contain' => []
        ]);
        $tripitoesSlider = $this->TripitoesSliders->patchEntity($tripitoesSlider, $this->request->data);
        $tripitoesSlider->is_deleted=1;
        if ($this->TripitoesSliders->save($tripitoesSlider)) {
            $this->Flash->success(__('The slider has been deleted.'));
        } else {
            $this->Flash->error(__('The slider could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'view']);
    }
}
