<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TripitoesSliders Model
 *
 * @method \App\Model\Entity\TripitoesSlider get($primaryKey, $options = [])
 * @method \App\Model\Entity\TripitoesSlider newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TripitoesSlider[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesSlider|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TripitoesSlider patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesSlider[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesSlider findOrCreate($search, callable $callback = null, $options = [])
 */
class TripitoesSlidersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tripitoes_sliders');
        $this->displayField('title');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create'); 

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name'); 
        $validator
            ->requirePresence('location', 'create')
            ->notEmpty('location');

        $validator
            ->requirePresence('link', 'create')
            ->notEmpty('link');

        $validator
            ->requirePresence('image_url', 'create')
            ->notEmpty('image_url');

        /*$validator
            ->dateTime('created_on')
            ->requirePresence('created_on', 'create')
            ->notEmpty('created_on');

        $validator
            ->boolean('is_deleted')
            ->requirePresence('is_deleted', 'create')
            ->notEmpty('is_deleted');
*/
        return $validator;
    }
}
